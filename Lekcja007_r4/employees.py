import csv
from pathlib import Path


def process_row(row):
    row["salary"] = float(row["salary"])
    return row


file_path = Path.home() / r"C:\Users\barto\OneDrive\Pulpit\ZR\Lekcja007_r4" / "employees.csv"
file = file_path.open(mode="r", encoding="utf-8", newline="")
reader = csv.DictReader(file)
for row in reader:
    print(process_row(row))
file.close()

people = [
    {"name": "Veronica", "age": 29},
    {"name": "Aundrey", "age": 32},
    {"name": "Sam", "age": 24}
]

file_path = Path.home() / r"C:\Users\barto\OneDrive\Pulpit\ZR\Lekcja007_r4" / "people.csv"
file = file_path.open(mode="w", encoding="utf-8", newline="")
writer = csv.DictWriter(file, fieldnames=["name", "age"])
writer.writeheader()
writer.writerows(people)
file.close()

