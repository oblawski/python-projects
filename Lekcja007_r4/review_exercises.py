import csv
from pathlib import Path

numbers = [
    [1, 2, 3, 4, 5],
    [6, 7, 8, 9, 10],
    [11, 12, 13, 14, 15],
]

# 1
file_path = Path.home() / r"C:\Users\barto\OneDrive\Pulpit\ZR\Lekcja007_r4" / "numbers.csv"
file = file_path.open(mode="w", encoding="utf-8", newline="")
writer = csv.writer(file)
for row in numbers:
    writer.writerow(row)
# writer.writerows(numbers) also ok!
file.close()

# 2
file_read = []
file = file_path.open(mode="r", encoding="utf-8", newline="")
reader = csv.reader(file)
for row in reader:
    int_row = [int(num) for num in row]
    file_read.append(int_row)
print(file_read)
file.close()

# 3
favorite_colors = [
    {"name": "Joe", "favorite_color": "blue"},
    {"name": "Anne", "favorite_color": "green"},
    {"name": "Bailey", "favorite_color": "red"},
]

file_path = Path.home() / r"C:\Users\barto\OneDrive\Pulpit\ZR\Lekcja007_r4" / "favorite_colors.csv"
file = file_path.open(mode="w", encoding="utf-8", newline="")
writer = csv.DictWriter(file, fieldnames=["name", "favorite_color"])
writer.writeheader()
writer.writerows(favorite_colors)
file.close()

# 4
favorite_colors_lst = []
file = file_path.open(mode="r", encoding="utf-8", newline="")
reader = csv.DictReader(file)
for row in reader:
    favorite_colors_lst.append(row)
print(favorite_colors_lst)
file.close()
