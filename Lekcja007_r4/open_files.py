from pathlib import Path
#
# path = Path.home() / "hello.txt"
# path.touch()
# # file = path.open(mode="r", encoding="utf-8")
# # file.close()
# #
# # file_path = "C:/Users/barto/hello.txt"
# # file = open(file_path, mode="r", encoding="utf-8")
# # file.close()
#
# # with path.open(mode="r", encoding="utf-8") as file:
# #     # do something with file
# #
# # with open(file_path, mode="r", encoding="utf-8") as file:
# #     # do something with file
#
# # with path.open(mode="r", encoding="utf-8") as file:
# #     for line in file.readlines():
# #         print(line, end="")
#
# with path.open(mode="w", encoding="utf-8") as file:
#     file.write("Hi there!")
#
# with path.open(mode="r", encoding="utf-8") as file:
#     text = file.read()
# print(text)
#
# with path.open(mode="a", encoding="utf-8") as file:
#     file.write("\nHello, there!")
#
# with path.open(mode="r", encoding="utf-8") as file:
#     text = file.read()
# print(text)
#
# lines_of_text = [
#     "text one hello\n",
#     "text two hello\n",
#     "text three hello\n"
# ]
#
# with path.open(mode="w", encoding="utf-8") as file:
#     file.writelines(lines_of_text)
#
# with path.open(mode="r", encoding="utf-8") as file:
#     text = file.read()
# print(text)
#
# path = Path.home() / "new_text.txt"
# with path.open(mode="w", encoding="utf-8") as file:
#     file.write("Hi my friend!")
#
# with path.open(mode="r", encoding="utf-8") as file:
#     text = file.read()
# print(text)
#
# path = Path.home() / "new_folder" / "new_text.txt"
# path.parent.mkdir(parents=True, exist_ok=True)
# with path.open(mode="w", encoding="utf-8") as file:
#     file.write("Hihihihi")
#
# with path.open(mode="r", encoding="utf-8") as file:
#     text = file.read()
# print(text)

# starships = [
#     "Discovery\n",
#     "Enterprise\n",
#     "Defiant\n"
#     "Voyager\n"
# ]
# path = Path.home() / "starships.txt"
# with path.open(mode="w", encoding="utf-8") as file:
#     file.writelines(starships)
#
# with path.open(mode="r", encoding="utf-8") as file:
#     for starship in file.readlines():
#         print(starship, end="")
#
# print()
# with path.open(mode="r", encoding="utf-8") as file:
#     for word in file.readlines():
#         if word.startswith("D"):
#             print(word, end="")
# temperature_readings = [68, 65, 68, 70, 74, 72]
# path = Path.home() / "temperatures.csv"
# with path.open(mode="w", encoding="utf-8") as file:
#     file.write(str(temperature_readings[0]))
#     for temp in temperature_readings[1:]:
#         file.write(f",{temp}")
# with path.open(mode="r", encoding="utf-8") as file:
#     txt = file.read()
# print(txt)
# temperatures = txt.split(",")
# print(temperatures)
# int_temperatures = [int(temp) for temp in temperatures]
# print(int_temperatures)