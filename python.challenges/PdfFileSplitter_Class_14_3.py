from PyPDF2 import PdfReader, PdfWriter
from pathlib import Path


class PdfFileSplitter:
    def __init__(self, pdf_reader):
        self.writer1 = None
        self.writer2 = None
        self.pdf_reader = PdfReader(pdf_reader)

    def split(self, breakpoint_page):
        self.writer1 = PdfWriter()
        self.writer2 = PdfWriter()
        for page in self.pdf_reader.pages[:breakpoint_page]:
            self.writer1.add_page(page)
        for page in self.pdf_reader.pages[breakpoint_page:]:
            self.writer2.add_page(page)

    def write(self, file_name):
        with Path(file_name + "_1.pdf").open(mode="wb") as output_file:
            self.writer1.write(output_file)
        with Path(file_name + "_2.pdf").open(mode="wb") as output_file:
            self.writer2.write(output_file)


pdf_splitter = PdfFileSplitter(Path.home() / "OneDrive" / "Pulpit" / "ZR" / "Lekcja008" /
                                "practice_files" / "Pride_and_Prejudice.pdf")

pdf_splitter.split(150)
pdf_splitter.write("latwo")