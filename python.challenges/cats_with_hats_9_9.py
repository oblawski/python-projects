cats_hats = {}
cats_with_hats_list = []


def number_of_cats_w_no_hat(cats_amount):
    for cats in range(1, cats_amount + 1):
        cats_hats[cats] = "NO"


def get_cats_with_hats(rounds):
    for i in range(1, rounds + 1):
        for cat in cats_hats:
            if cat % i == 0:
                if cats_hats[cat] == "NO":
                    cats_hats[cat] = "YES"
                else:
                    cats_hats[cat] = "NO"
            else:
                continue
    for cat in cats_hats:
        if cats_hats[cat] == "YES":
            cats_with_hats_list.append(cat)


number_of_cats_w_no_hat(100)
get_cats_with_hats(100)
print(f"List of cats with hats: {cats_with_hats_list}")
