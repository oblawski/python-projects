import statistics
universities = [
    ['California Institute of Technology', 2175, 37704],
    ['Harvard', 19627, 39849],
    ['Massachusetts Institute of Technology', 10566, 40732],
    ['Princeton', 7802, 37000],
    ['Rice', 5879, 35551],
    ['Stanford', 19535, 40569],
    ['Yale', 11701, 40500]
]


def enrollment_stats(list_of_lists):
    students_list = [item[1] for item in list_of_lists]
    tuition_list = [item[2] for item in list_of_lists]
    return students_list, tuition_list


def mean(lst):
    return sum(lst) / len(lst)


def median(lst):
    return statistics.median(lst)


print("******************************")
print(f"Total students: {sum(enrollment_stats(universities)[0]):,}")
print(f"Total tuition: $ {sum(enrollment_stats(universities)[1]):,}")
print()
print(f"Student mean: {mean(enrollment_stats(universities)[0]):,.2f}")
print(f"Student median: {median(enrollment_stats(universities)[0]):,}")
print()
print(f"Tuition mean: ${mean(enrollment_stats(universities)[1]):,.2f}")
print(f"Tuition median: ${median(enrollment_stats(universities)[1]):,}")
print("******************************")

