import random

capitals_dict = {
    'Alabama': 'Montgomery',
    'Alaska': 'Juneau',
    'Arizona': 'Phoenix',
    'Arkansas': 'Little Rock',
    'California': 'Sacramento',
    'Colorado': 'Denver',
    'Connecticut': 'Hartford',
    'Delaware': 'Dover',
    'Florida': 'Tallahassee',
    'Georgia': 'Atlanta',
}

state, capital = random.choice(list(capitals_dict.items()))

while True:
    user_input = input(f"You drew {state}. What is the capital of this state? Type 'exit' to quit. ").lower()
    if user_input == capital.lower():
        print("Correct")
        break
    elif user_input == "exit":
        print(f"Correct answer is {capital}")
        print("Goodbye")
        break








