import random


def coin_flip():
    """Return randomly heads or tails"""
    if random.randint(0, 1) == 1:
        return "heads"
    else:
        return "tails"


trials = 10_000
flips = 0

for trial in range(trials):
    if coin_flip() == "heads":
        flips += 1
        while coin_flip() == "heads":
            flips += 1
        flips += 1
    else:
        flips += 1
        while coin_flip() == "tails":
            flips += 1
        flips += 1

print(f"Total flips: {flips}")
print(f"Average flips: {flips / trials}")

