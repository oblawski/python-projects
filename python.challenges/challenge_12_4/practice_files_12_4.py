from pathlib import Path

main_dir = Path(r"/python.challenges/challenge_12_4/practice_files")
img_folder = Path(r"/python.challenges/challenge_12_4/practice_files") / "images"
img_folder.mkdir(exist_ok=True)

for image in main_dir.rglob("image*.*"):
    image.replace(img_folder / image.name)
