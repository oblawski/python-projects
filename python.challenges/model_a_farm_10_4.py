class Animal:
    food_count = 0
    position = 0

    def __init__(self, name, age):
        self.name = name
        self.age = age

    def __str__(self):
        return f"{self.name} is {self.age} years old."

    def speak(self, sound):
        """<name> says <sound>"""
        return f"{self.name} says {sound}"

    def walking(self, squares):
        """moving by <squares> squares forward"""
        self.position += squares
        return self.position

    def eating(self):
        """eating 1 portion of food"""
        self.food_count += 1
        return f"{self.name} is eating. Nam nam."

    def is_hungry(self):
        """check if <name> is full and show fullness level"""
        print(f"Fullness level: {self.food_count}")
        if self.food_count >= 3:
            return f"{self.name} is full!"
        elif 3 > self.food_count > 0:
            return f"{self.name} would like to eat something!"
        else:
            return f"{self.name} is hungry!"


class Pig(Animal):
    def speak(self, sound="Chrum"):
        return super().speak(sound)


class Cow(Animal):
    def speak(self, sound="Muu"):
        return super().speak(sound)


class Chicken(Animal):
    def speak(self, sound="Koko"):
        return super().speak(sound)


peppa = Pig("Peppa", 9)
krowa = Cow("Krowa", 20)
kurczak = Chicken("Kurczak", 10)
print(peppa.speak())
print(kurczak.eating())
print(kurczak.is_hungry())
print(kurczak.eating())
print(kurczak.eating())
print(kurczak.eating())
print(kurczak.is_hungry())
print(krowa.is_hungry())
print(peppa.walking(3))
print(peppa.walking(3))
print(peppa.walking(-4))

