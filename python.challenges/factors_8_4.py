user_int = int(input("Enter a positive integer: "))
for i in range(1, user_int + 1):
    if user_int % i == 0:
        print(f"{i} is a factor of {user_int}")
