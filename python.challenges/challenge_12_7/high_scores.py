import csv
from pathlib import Path


def find_highest_score(scores):
    highest_scores = {}
    for element in scores:
        name = element["name"]
        score = element["score"]

        if name not in highest_scores or score > highest_scores[name]:
            highest_scores[name] = score

    result = [{"name": name, "high_score": score} for name, score in highest_scores.items()]
    return result


file_list = []
path_file = Path.cwd() / "practice_files" / "scores.csv"
with path_file.open(mode="r", encoding="utf-8", newline="") as file:
    reader = csv.DictReader(file)
    for row in reader:
        file_list.append(row)
result = find_highest_score(file_list)

path_new_file = Path.cwd() / "practice_files" / "high_scores.csv"
with path_new_file.open(mode="w", encoding="utf-8", newline="") as file:
    writer = csv.DictWriter(file, fieldnames=["name", "high_score"])
    writer.writeheader()
    writer.writerows(result)

print(Path.cwd())
