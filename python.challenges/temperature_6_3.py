def convert_cel_to_far(cel_degrees):
    """Return the Celsius temperature cel_degrees converted to Fahrenheit."""
    far_degrees = float(cel_degrees) * 9 / 5 + 32
    return float(far_degrees)


def convert_far_to_cel(far_degrees):
    """Return the Fahrenheit temperature far_degrees converted to Celsius."""
    cel_degrees = (float(far_degrees) - 32) * 5 / 9
    return float(cel_degrees)


user_far_dg = input("Enter a temperature in degrees F: ")
c_degrees = convert_far_to_cel(user_far_dg)
print(f"{user_far_dg} degrees F = {c_degrees:.2f} degrees C")

user_c_dg = input("Enter a temperature in degrees C: ")
f_degrees = convert_cel_to_far(user_c_dg)
print(f"{user_c_dg} degrees C = {f_degrees:.2f} degrees F")