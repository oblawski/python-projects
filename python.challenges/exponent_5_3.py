num1 = input("Enter a base: ")
num2 = input("Enter an exponent: ")

base = float(num1)
exponent = float(num2)

print(f"{base} to the power of {exponent} = {base ** exponent}")
