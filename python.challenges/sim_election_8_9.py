import random

trials = 10_000
can_A_win = 0


def region_win(percent):
    """check if candidate A win in region"""
    if random.randint(1, 100) <= percent:
        return "win"


for trial in range(0, trials):
    can_A_win_condition = 0  # must be at least 2 to give win to candidate A.
    if region_win(87) == "win":
        can_A_win_condition += 1
    if region_win(65) == "win":
        can_A_win_condition += 1
    if region_win(17) == "win":
        can_A_win_condition += 1

    if can_A_win_condition >= 2:
        can_A_win += 1

print(f"Candidate A wins {can_A_win / trials:.2%} times.")
