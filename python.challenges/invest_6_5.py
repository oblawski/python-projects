def invest(amount, rate, years):
    """Display growth of an investment year by year"""
    for i in range(years):
        amount = amount + (amount * rate)
        print(f"Year {i+1}: ${amount:,.2f}")


amount = float(input("Enter an amount: "))
rate = float(input("Enter a rate: "))
years = int(input("Enter a years: "))

invest(amount, rate, years)
