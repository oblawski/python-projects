# print(type("hi"))
#
# word = "letters"
# print(len(word))
# print(len("letters"))
#
# print('String with "double quotation mark"')
# print("String with apostroph'e inside")
#
# string = "bobiczek"
# print(len(string))
# str1 = "bobi"
# str2 = "czek"
# print(str1 + str2)
# print(str1 + " " + str2)
# lol = "bazinga"
# print(lol[2:6])
#
# animal = "AnimALs"
# print(animal.lower())
#
# big = "animal"
# print(big.upper())
#
# string1 = "    Filet Mignon"
# string2 = "Brisket    "
# string3 = "  Cheeseburger   "
#
# print(string1.lstrip())
# print(string2.rstrip())
# print(string3.strip())
#
# string1 = "Becomes"
# string2 = "becomes"
# string3 = "BEAR"
# string4 = "  bEautiful"
# print(string1.startswith("be"))
# print(string2.startswith("be"))
# print(string3.startswith("be"))
# print(string4.startswith("be"))
#
# print(input("Say hi!"))
#
# prompt = "Hey, whats up?"
# user_input = input(prompt)
# print("You said: " + user_input)

# my_input = input("Say something")
#
# print(my_input)
# print(my_input.lower())
# print(len(my_input))

# num = 10
# print("Hi im " + str(num) + " bartek")
# print(f"Hi im {num} bartek")
#
# n = "10"
# m = int(n)
# print(m**3)
#
# n = "10"
# m = float(n)
# print(m*2)
#
# x = "10"
# y = 15
# print(x + str(y))
#
# num1 = input("number 1: ")
# num2 = input("number 2: ")
# num1_int = float(num1)
# num2_int = float(num2)
# print(num1_int * num2_int)

# n = 3
# m = 4
#
# print(f"{n} times {m} is {n * m}")
# print("{} times {} is {}".format(n, m, n * m))
#
# weight = 0.2
# animal = "newt"
# print(str(weight) + " kg is the weight of the " + animal + ".")
# print(f"{weight} kg is the weight of the {animal}.")
# print("{} kg is the weight of the {}.".format(weight, animal))
#
# phrase = "the surprise is in here somewhere"
# print(phrase.find("surprise"))
#
# my_story = "Im telling you the truth; nothing but the truth!"
# print(my_story.replace("the truth", "lies"))
# print(my_story)
#
# print("AAA".find("a"))
# print("Somebody said something to Samantha".replace("s", "x"))
# x = input("say something fun")
# print(x.find("a"))

# print(1e3)
# print(-2e390)

# num1 = 25000000
# num2 = 25_000_000
# print(num1)
# print(num2)
#
# num = 175e3
# print(num)
#
# print(2e308)

num = 2.0
print(num.is_integer())
print(pow(2,3,3))
print(abs(-9))
print(round(3.141527, 2))

# num = input("enter a number ")
# result = float(num)
# print(f"{num} rounded to 2 decimal places is {round(float(num), 2)}")

# num = input("Enter a number: ")
# result = float(num)
# print(f"The absolute value of the number {num} is {abs(result)}")
#
# num1 = input("Enter a num 1: ")
# num2 = input("Enter a num 2: ")
# result = float(num1) - float(num2)
# print(f"The difference between {num1} and {num2} is an integer? {result.is_integer()}!")
#
# n = 7234234.125234
# print(f"The value of n is {n:,.2f}")
#
# ratio = 0.9
# print(f"Over {ratio:.1%} of Pythonistas say 'Real Python rocks!'")
#
# balance = 2000.0
# spent = 256.35
# remaining = balance - spent
# print(f"After spending ${spent:,.2f}, I was left with ${remaining:,.2f}")

n = 3**.125
print(f"{n:.3f}")

balance = 150000
print(f"${balance:,.2f}")

m = 2/10
print(f"{2/10:.0%}")

