def cube(x):
    result = pow(x, 3)
    return result


def greet(name):
    print(f"Hello, {name}!")


# num = float(input("Enter positive: "))
#
# while num<=0:
#     print("Thats not positive number!")
#     num = float(input("Enter positive: "))

# for letter in "Python":
#     print(letter)

# word = "Python"
# index = 0
#
# while index < len(word):
#     print(word[index])
#     index += 1

# for n in range(10, 20):
#     print(n+n)

# amount = float(input("Enter an amount: "))
#
# for num_people in range(2,6):
#     print(f"{num_people} people ${amount / num_people:,.2f} each")

# for n in range(5):
#     print((n + 1) * "*")

# for n in range(1, 4):
#     for j in range(4, 7):
#         print(f"n = {n} and j= {j}")

# for i in range(2, 11):
#     print(i)
#
# i = 2
# while i < 11:
#     print(i)
#     i += 1


# def doubles(number):
#     result = number * 2
#     return result
#
#
# my_num = 2
# for i in range(0, 3):
#     my_num = doubles(my_num)
#     print(my_num)

x = "Hello, World"

# def fun():
#     x = 2
#     print(f"Inside 'func' x has the value {x}")
#
#
# fun()
# print(f"Outside the 'func' x has the value {x}")

#
# x = 5
# y = 3
#
#
# def outer_func():
#
#     def inner_func():
#         z = x + y
#         return z
#
#     return inner_func()
#
#
# print(outer_func())


# total = 0
#
#
# def add_to_total(n):
#     global total
#     total = total + n
#
#
# add_to_total(5)
# print(total)


def add_underscores(word):
    new_word = "_"
    for letter in word:
        new_word = new_word + letter + "_"
    return new_word


phrase = "hello"
print(add_underscores(phrase))
