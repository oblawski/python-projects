import copy
# print(1 <= 1) #t
# print(1 != 1) #f
# print(1 != 2) #t
# print("good" != "bad") #t
# print("good" != "Good") #t
# print(123 == "123") #f
import random

# print((1 <= 1) and (1 != 1)) #F
# print(not (1 != 2)) #F
# print(("good" != "bad") or False) #T
# print(("good" != "Good") and not (1 == 1)) #F

# print(False == (not True))
# print(True and (False == (True and False)))
# print(not (True and "A" == "B"))

# word = input("Enter a random word: ")
# if len(word) > 5:
#     print("Your input is greater than 5 characters long.")
# elif len(word) == 5:
#     print("Your input is 5 characters long.")
# else:
#     print("Your input is less than 5 characters long.")


# while True:
#     number = int(input("I'm thinking of a number between 1 and 10. Guess which one. "))
#     if number == 3:
#         print("You win!")
#         break
#     else:
#         print("Wrong! Try again.")

# for n in range(3):
#     password = input("Password: ")
#     if password == "lol":
#         print("Correct password.")
#         break
#     print("Password incorrect")
#
# else:
#     print("Suspicious activity. The authorities have been alerted.")

# while True:
#     word = input("Enter: ")
#     if word.lower() == "q":
#         break

# for i in range(1, 51):
#     if i % 3 == 0:
#         continue
#     print(i)

# try:
#     number = int(input("Enter an integer: "))
# except ValueError:
#     print("That was not an integer")

# number = input("Enter an integer: ")
# try:
#     print(int(number))
# except ValueError:
#     print("Try again")

# string = input("Enter a string: ")
# while True:
#     number = input("Enter a character index in string: ")
#     try:
#         print(f"{number} character in {string} is {string[int(number)]}")
#         break
#     except ValueError:
#         print("Invalid number!")
#     except IndexError:
#         print("Index is out of bound!")


# def coin_flip():
#     heads = 0
#     tails = 0
#     attempts = 10_000
#
#     for attempt in range(attempts):
#         if random.randint(0, 1) == 0:
#             heads += 1
#         else:
#             tails += 1
#
#     print(f"Number of heads: {heads}")
#     print(f"Number of tails: {tails}")
#     print(f"Heads ratio is equal to: {(heads / attempts):.2%}")
#
# coin_flip()
#
# sum = 0
#
# for trials in range(10_000):
#     sum += random.randint(1, 6)
#
# print(round(sum / 10_000))

# cardinal_numbers = ("first", "second", "third")
# print(cardinal_numbers[1])
#
# position1, position2, position3 = "first", "second", "third"
# print(position1)
#
# my_name = tuple("Bartosz")
# print(my_name)
#
# print("x" in my_name)
#
# name = my_name[1:]
# print(name)

# board = "x,o,x"
# boards = board.split(",")
# print(boards)
#
# lst = [1, 2, 3]
# print(sum(lst))
#
# numbers = [1, 2, 3, 4, 5]
# squares = [num**2 for num in numbers]
# print(squares)
#
# str_numbers = ["1.4", "2.5", "42.3"]
# float_numbers = [float(num) for num in str_numbers]
# print(float_numbers)

# food = ["rice", "beans"]
# print(food)
# food.append("broccoli")
# print(food)
# food.extend(("bread", "pizza"))
# print(food)
# print(food[:2])
# print(food[-1])
# breakfast = "eggs,fruit,orange juice".split(",")
# print(breakfast)
# print(len(breakfast) == 3)
# lengths = [len(item) for item in breakfast]
# print(lengths)


# # copy list
# matrix1 = [[1, 2], [3, 4]]
# matrix2 = matrix1[:]
# matrix2[0] = [5, 6]
# print(matrix2)
# print(matrix1)
#
# matrix3 = copy.deepcopy(matrix2)
# matrix3[1][0] = 3
# print(matrix3)

# data = ((1, 2), (3, 4))
# index = 1
# for item in data:
#     print(f"Row {index} sum: {sum(item)}")
#     index += 1
#
# numbers = [4, 3, 2, 1]
# copy_numbers = numbers[:]
# numbers.sort()
# print(numbers)
#
# """dictionary"""
# capitals = {
#     "California": "Sacramento",
#     "New York": "Albany",
#     "Texas": "Austin",
# }
#
# key_value_pairs = (
#     ("California", "Sacramento"),
#     ("New York", "Albany"),
#     ("Texas", "Austin")
# )
# capitals1 = dict(key_value_pairs)
# print(capitals["New York"])
# print(capitals1["Texas"])
#
# capitals["Colorado"] = "Denver"
# print(capitals)
# capitals["Texas"] = "Houston"
# print(capitals)
#
# del capitals["Texas"]
# print(capitals)
# print("Arizona" in capitals)
# print("New York" in capitals)
#
# capitals["Arizona"] = "idk"
#
# if "Arizona" in capitals:
#     # Print only if the "Arizona key exists"
#     print(f"The capital of Arizona is {capitals['Arizona']}")
#
# for key in capitals:
#     print(key)
#
# for state in capitals:
#     print(f"The capital of {state} is {capitals[state]}")
#
# for state, capital in capitals.items():
#     print(f"The capital of {state} is {capital}")
#
# captains = {}
# captains["Enterprise"] = "Picard"
# captains["Voyager"] = "Janeway"
# captains["Defiant"] = "Sisko"
#
# if "Enterprise" in captains:
#     print(f"The value for 'Enterprise' is {captains['Enterprise']}")
# else:
#     captains["Enterprise"] = "unknown"
#
# if "Discovery" in captains:
#     print(f"The value for 'Discovery' is {captains['Discovery']}")
# else:
#     captains["Discovery"] = "unknown"
#
# for ship, captain in captains.items():
#     print(f"The {ship} is captained by {captain}")
#
# del captains["Discovery"]
# print(captains)
#
# cpts = dict(
#     [("Enterprise", "Picard"),
#      ("Voyager", "Janeway"),
#      ("Defiant", "Sisko"),
#      ]
# )
# print(cpts)

x = [10, [3.141, 20, [30, 'baz', 2.718]], 'foo']
a = (1, 3)
if a == tuple(a):
    print(a)