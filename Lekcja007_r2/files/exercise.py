import pathlib
import shutil

new_dir = pathlib.Path.home() / "my_folder"
new_dir.mkdir(exist_ok=True)

files = [
    new_dir / "file1.txt",
    new_dir / "file2.txt",
    new_dir / "image1.png"
]

for file in files:
    file.touch()

img_dir = new_dir / "images"
img_dir.mkdir(exist_ok=True)

source = new_dir / "image1.png"
destination = new_dir / "images" / "image1.png"
if not destination.exists():
    source.replace(destination)

file_1 = new_dir / "file1.txt"
file_1.unlink()

new_directory = pathlib.Path.home() / "new_directory"
shutil.rmtree(new_directory)
shutil.rmtree(new_dir)