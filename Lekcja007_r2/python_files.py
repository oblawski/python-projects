from pathlib import Path

"""
path.anchor - if file is absolute, it returns the root directory of the path file as a string
path.name - returns the name of file/dir that the path points to
hello.txt - hello is a stem, txt is a suffix
path.stem - returns stem
path.suffix - returns suffix

path.exist() - check if file exist
path.is_file() - check if the path is a file
path.is_dir() - check if the path is a directory

path.mkdir() - create a folder
path.mkdir(exist_ok=True) - if the folder already exist, avoid raising the FileExistsError
path.mkdir(parents=True) - if the parent folder doesnt exist, it creates any folder above the path point

for path in new_dir.iterdir(): - allow to iterable over the directory
for path in new_dir.glob("*.txt"): - searching any .txt file in a directory
"""


# path = pathlib.Path("C:/Users/barto/OneDrive/Pulpit/ZR/Lekcja007_r2/files/hello.txt")
# cwd = pathlib.Path.cwd()
# home = pathlib.Path.home()
# print(cwd)  # current working directory
# print(home)  # home directory

# path = pathlib.Path.home() / "hello.txt"
# print(path)
# print(list(path.parents))
#
# for directory in path.parents:  # .parents is shortcut for .parents[0]
# print(directory)

# path = pathlib.Path.home() / "hello.txt"
# home = pathlib.Path.home()
# print(path.exists())
# print(path.is_file())
# print(path.is_dir())
# print(home.is_dir())
#
# file_path = pathlib.Path.home() / "my_folder" / "my_file.txt"
# path.touch() - creates a file for a path
# print(file_path.exists())
# print(file_path.name)
# print(file_path.parent.name)

new_dir = Path.home() / "new_directory"  # setting a new folder "new_directory" path
new_dir.mkdir(exist_ok=True)  # create a folder
# print(new_dir.exists())
#
# nested_dir = new_dir / "folder_a" / "folder_b"
# nested_dir.mkdir(parents=True, exist_ok=True)
# # print(nested_dir)
#
# file_path = new_dir / "folder_c" / "file2.txt"
# # print(file_path.exists())
#
# """create a new file, check if already exist"""
# file_path.parent.mkdir(exist_ok=True)
# file_path.touch()

# for path in new_dir.iterdir():
#     print(path)
#
# print(list(new_dir.iterdir()))  # allowed
#
# for path in new_dir.glob("*.txt"):
#     print(path)

"""create n files"""
# paths = [
#     new_dir / "program1.py",
#     new_dir / "program2.py",
#     new_dir / "folder_a" / "program3.py",
#     new_dir / "folder_a" / "folder_b" / "image1.jpg",
#     new_dir / "folder_a" / "folder_b" / "image2.png"
# ]
# for path in paths:
#     path.touch()

# print(list(new_dir.glob("*.py")))
# print(list(new_dir.glob("*1*")))
# print(list(new_dir.glob("program?.py")))
# print(list(new_dir.glob("?older_?")))
# print(list(new_dir.glob("*1.??")))
#
# print(list(new_dir.glob("program[13].py")))
# print(list(new_dir.glob("**/*.py")))
# print(list(new_dir.rglob("*.py")))

# """change location of a file / folder"""
# source = new_dir / "file1.txt"
# destination = new_dir / "folder_a" / "file1.txt"
# if not destination.exists():
#     source.replace(destination)

# """rename file / folder"""
# source = new_dir / "folder_c"
# destination = new_dir / "folder_d"
# if not destination.exists():
#     source.replace(destination)

# """delete file"""
# file_path = new_dir / "program1.py"
# file_path.unlink(missing_ok=True)
#
# """delete folder"""
# folder_d = new_dir / "folder_d"
# for path in folder_d.iterdir():
#     path.unlink()
#
# folder_d.rmdir()

import shutil
folder_a = new_dir / "folder_a"
# shutil.rmtree(folder_a)

print(list(new_dir.glob("image.*")))