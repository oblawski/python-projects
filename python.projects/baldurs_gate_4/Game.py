import time
import Character
import Control
import Items


def game():
    """game order"""
    Control.scenario("0")
    Character.player.set_name(input("\nWhat is your name? "))
    time.sleep(1)
    Control.scenario("1")
    time.sleep(2)
    Control.fight(Character.skeleton)
    Control.scenario("2")
    Control.add_to_inventory("health potion")
    Control.information_in_red("\nYou can find your health potion in equipment (2)"
                               " and use it to heal!")

    while True:
        print()
        Control.status()
        Control.inventory()

        choice = input(f"What do you want to do, {Character.player.name}?\n"
                       f"1 - Check my items description.\n"
                       f"2 - Use or equip an item.\n"
                       f"3 - Go south, let's check the battlefield. \n"
                       f"4 - Go north, where you feel your destination awaits you.\n"
                       f"5 - Quit game\n")
        match choice:
            case "1":
                Control.check_item_description()
            case "2":
                answer = input("Choose item to use or equip: (if you don't want to use any item"
                               " type 'back').\n")
                if answer.lower() == "back":
                    continue
                elif answer not in Items.inventory:
                    print(f"The {answer} is not found in your inventory.")
                else:
                    Control.item_use(answer)
            case "3":
                print("You have decided to go south.")
                if "key" in Items.inventory:
                    print("You've already visited this place, it's time to move forward!\n")
                else:
                    Control.scenario("4")
                    Control.open_chest()
            case "4":

                if "key" not in Items.inventory:
                    Control.scenario("6")
                    Control.scenario("7")
                elif "lucky ring" not in Items.inventory:
                    Control.scenario("6")
                    Control.scenario("8")
                    Control.scenario("9")
                    Control.add_to_inventory("lucky ring")
                    Control.information_in_red("\nYou received the mysterious Lucky Ring.\n")
                elif not Items.ring.equipped and "lucky ring" in Items.inventory:
                    print("Firstly equip your lucky ring!\n")
                else:
                    Control.scenario("10")
                    Control.fight(Character.boss)
                    Control.scenario("11")
                    Control.information_in_red("\nYOU HAVE KILLED THE NECROMANCER AND SAVED THE WORLD!")
                    break
            case "5":
                break
            case "_":
                print("Wrong choice. Try again.\n")
