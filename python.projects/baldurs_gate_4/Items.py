class Items:
    def __init__(self, name, equipped):
        self.name = name
        self.equipped = equipped

    @staticmethod
    def get_attribute(obj, attribute_name):
        return getattr(obj, attribute_name, None)

    def set_equipped(self):
        """if equipped"""
        if not self.equipped:
            self.equipped = True
        else:
            self.equipped = False


sword = Items("sword", False)
ring = Items("lucky ring", False)

inventory = []
equipped = []
chest_1 = ["sword", "key"]
chest_2 = ["lucky ring"]

items_list = {"key": "This key is needed to open the boss room.",
              "health potion": "Regenerates 100% health points.",
              "lucky ring": "Increases your health points by 50.",
              "sword": "Sharp weapon. +15 attack.",
              "sword (equipped)": "Sharp weapon. +15 attack."}
