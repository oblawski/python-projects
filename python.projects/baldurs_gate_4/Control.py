import csv
from pathlib import Path
import Items
import time
import Character
from colorama import init, Fore


def information_in_red(text):
    """prints information text in red"""
    init()
    print(Fore.RED + text + Fore.RESET)


def scenario(number):
    """prints specific scenario"""
    path_file = Path.cwd() / "scenario.csv"
    with path_file.open(mode="r", encoding="utf-8", newline="") as file:
        reader = csv.DictReader(file, fieldnames=["scenario", "describe"])
        for text in reader:
            if text["scenario"] == number:
                for char in text["describe"]:
                    print(char, end='', flush=True)
                    time.sleep(0.03)


def inventory():
    """displays an inventory"""
    if len(Items.inventory) == 0:
        print("Your inventory is empty.\n")
    else:
        result = ", ".join(Items.inventory)
        print(f"Your inventory: {result}\n")


def add_to_inventory(item):
    """adds item to inventory"""
    Items.inventory.append(item)


def open_chest():
    """displays a list of items in chest"""
    while True:
        answer = input("\nWould you like to open chest and collect all of items inside? Y/N"
                       " (if not, you're going back to the camp.\n")
        if answer.lower() == "y":
            result = ", ".join(Items.chest_1)
            print(f"This chest contains: {result}")
            add_to_inventory("sword")
            add_to_inventory("key")
            scenario("5")
            information_in_red("\nNow you can equip your sword in menu!")
            break
        elif answer.lower() == "n":
            print("You have decided to go back to the camp.")
            break
        else:
            print("Wrong answer, try again.")


def check_item_description():
    """checks item description"""
    while True:
        if len(Items.inventory) == 0:
            print("You have no items to check.")
            break
        else:
            item_name = input(f"Select an item to see description ('back' to return to menu).\n").lower()
            if item_name.lower() in Items.inventory:
                print(Items.items_list[item_name])
                break
            elif item_name.lower() == "back":
                break
            else:
                print(f"The {item_name.lower()} is not found in your inventory. Try again.\n")


def status():
    """shows current hp and attack"""
    print(f"Your current hp is: {Character.player.hp}")
    print(f"Your current attack rate is: {Character.player.atk}")


def item_use(item_name):
    """uses item"""
    match item_name.lower():
        case "health potion":
            if Items.ring.equipped:
                Character.player.set_hp(150)
                Items.inventory.remove("health potion")
            else:
                Character.player.set_hp(100)
                Items.inventory.remove("health potion")
            information_in_red(f"You have healed 100% of your hp! Your current hp is: {Character.player.hp}.")
        case "sword":
            if not Items.sword.equipped:
                Character.player.set_atk(25)
                Items.sword.set_equipped()
                information_in_red("Sword equipped!")
            else:
                Character.player.set_atk(10)
                Items.sword.set_equipped()
                information_in_red("Sword unequipped!")
        case "lucky ring":
            if not Items.ring.equipped:
                Character.player.set_hp(150)
                Items.ring.set_equipped()
                information_in_red("Lucky ring equipped!")
            else:
                Character.player.set_hp(100)
                Items.ring.set_equipped()
                information_in_red("Lucky ring unequipped!")
            print(f"Your current max hp is: {Character.player.hp}")
        case "key":
            information_in_red("You cannot equip this item!")
        case "_":
            print(f"You don't have item like {item_name} or this item doesn't exist.")


def fight(monster):
    """fight process with specific enemy"""
    player_current_hp = Character.player.hp
    player_full_hp = Character.player.hp

    enemy_current_hp = Character.Character.get_attribute(monster, 'hp')
    enemy_full_hp = Character.Character.get_attribute(monster, 'hp')

    enemy_name = Character.Character.get_attribute(monster, 'name')
    player_name = Character.player.name

    enemy_atk = Character.Character.get_attribute(monster, 'atk')
    player_atk = Character.player.atk
    print(f"\n{player_name} is fighting {enemy_name}!\n")
    time.sleep(1)
    while True:
        print(f"{enemy_name} HP: {enemy_current_hp}/{enemy_full_hp}")
        time.sleep(0.5)
        print(f"{player_name} HP: {player_current_hp}/{player_full_hp}\n")
        time.sleep(0.5)
        print(f"{player_name} hits {enemy_name} for {player_atk} hp.\n")
        time.sleep(2)
        enemy_current_hp -= player_atk
        if enemy_current_hp <= 0:
            print(f"{player_name} defeated {enemy_name}!\n")
            Character.player.hp = player_current_hp
            break
        print(f"{enemy_name} HP: {enemy_current_hp}/{enemy_full_hp}")
        time.sleep(0.5)
        print(f"{player_name} HP: {player_current_hp}/{player_full_hp}\n")
        time.sleep(0.5)
        print(f"{enemy_name} hits {player_name} for {enemy_atk} hp.\n")
        player_current_hp -= enemy_atk
        time.sleep(2)
        if player_current_hp <= 0:
            print(f"{enemy_name} defeated {player_name}")
            break
