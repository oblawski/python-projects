
class Character:
    def __init__(self, name, hp, atk):
        self.name = name
        self.hp = hp
        self.atk = atk

    @staticmethod
    def get_attribute(obj, attribute_name):
        return getattr(obj, attribute_name, None)

    def set_name(self, value):
        """set name for object"""
        self.name = value

    def set_hp(self, value):
        """set hp for object"""
        self.hp = value

    def set_atk(self, value):
        """set atk for object"""
        self.atk = value


skeleton = Character("Skeleton", 50, 10)
boss = Character("Necromancer", 100, 20)
player = Character("Player", 100, 10)
