class HashTable:
    def __init__(self, size=10):
        self.size = size
        # Inicjalizacja tablicy (będącej listą list dla obsługi kolizji metodą łańcuchową)
        self.table = [[] for _ in range(self.size)]

    def hash_function(self, key):
        # Metoda do obliczania indeksu w tablicy dla danego klucza
        sum = 0
        for char in key:
            sum += ord(char)
        return sum % self.size

    def insert(self, key, value):
        # Metoda do dodawania pary klucz-wartość do tablicy haszującej
        hashed_key = self.hash_function(key)
        self.table[hashed_key].append((key, value))

    def remove(self, key):
        # Metoda do usuwania pary klucz-wartość na podstawie klucza
        hashed_key = self.hash_function(key)
        for index, (k, v) in enumerate(self.table[hashed_key]):
            if k == key:
                del self.table[hashed_key][index]

    def search(self, key):
        # Metoda do wyszukiwania wartości na podstawie klucza
        hashed_key = self.hash_function(key)
        for k, value in self.table[hashed_key]:
            if k == key:
                return value
        return "Podany klucz nie istnieje."

    def contains(self, key):
        # Metoda do sprawdzania, czy klucz znajduje się w tablicy
        hashed_key = self.hash_function(key)
        for i in self.table[hashed_key]:
            for k in i:
                if key in k:
                    return f"Klucz = {key} znajduje się w tablicy."

    def count_elements(self):
        # Metoda do zwracania liczby elementów w tablicy
        sum = 0
        for i in self.table:
            for k in i:
                if k == tuple(k):
                    sum += 1
        return sum


# Przykład użycia (będzie wymagał implementacji metod)
hash_table = HashTable(size=10)
hash_table.insert("klucz1", "wartość1")
hash_table.insert("klucz2", "wartość2")
hash_table.insert("klucz3", "wartość3")
print(hash_table.search("klucz2"))
hash_table.remove("klucz1")
print(hash_table.search("klucz1"))
print(hash_table.contains("klucz3"))
print(hash_table.count_elements())
