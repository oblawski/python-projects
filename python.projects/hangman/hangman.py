word = input("Hi! Welcome to the Hangman game. Enter a word to guess (word can not contain any numbers): ")

hidden_word = ["_" for letter in word.lower()]
user_word_list = []
life = 7


def word_done():
    """check if hidden word is already discovered and stop the program if true"""
    for i in hidden_word:
        if hidden_word.count("_"):
            return True
        else:
            print("You win!")
            return False


def check_letter_in_word():
    """check if guess is correct and add correct letters to the hidden word"""
    global life
    counter = 0
    if hidden_word.count(user_guess):
        print("You have already used this letter. Try again!")
        life -= 1
    for i in list(word):
        if user_guess == i:
            hidden_word[counter] = user_guess
            counter += 1
        else:
            counter += 1
    return " ".join(hidden_word)


def user_guess_list(lst):
    """show already used letters"""
    lst.append(user_guess)
    print(f'Already used letters: {", ".join(lst)}')


def lives():
    """check and subtract lives when guess is wrong"""
    global life
    if hidden_word.count(user_guess):
        print(f"You have {life} lives left.")
    else:
        life -= 1
        print(f"You have {life} lives left.")


def draw_hangman(life):
    """draw a hangman based on lives left"""
    if life == 6:
        print("________ ")
        print("|  | ")
        print("| ")
        print("| ")
        print("| ")
        print("| ")
    elif life == 5:
        print("________ ")
        print("|  | ")
        print("|  0 ")
        print("| ")
        print("| ")
        print("| ")
    elif life == 4:
        print("________ ")
        print("|  | ")
        print("|  0 ")
        print("| / ")
        print("| ")
        print("| ")
    elif life == 3:
        print("________ ")
        print("|  | ")
        print("|  0 ")
        print("| /| ")
        print("| ")
        print("| ")
    elif life == 2:
        print("________ ")
        print("|  | ")
        print("|  0 ")
        print("| /|\ ")
        print("| ")
        print("| ")
    elif life == 1:
        print("________ ")
        print("|  | ")
        print("|  0 ")
        print("| /|\ ")
        print("| / ")
        print("| ")
    elif life == 0:
        print("________ ")
        print("|  | ")
        print("|  0 ")
        print("| /|\ ")
        print("| / \ ")
        print("| ")


while True:
    if word_done():
        user_guess = input("Choose a letter to guess: ")
        user_guess_list(user_word_list)
        print(check_letter_in_word())
        lives()
        draw_hangman(life)
        if life == 0:
            print("You lose!")
            break
        print()
    else:
        break
