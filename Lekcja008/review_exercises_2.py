from PyPDF2 import PdfWriter, PdfReader
from pathlib import Path

pdf_path = (Path.home() / "OneDrive" / "Pulpit" / "ZR" / "Lekcja008" / "practice_files" / "Pride_and_Prejudice.pdf")
pdf_reader = PdfReader(pdf_path)

# 1
pdf_writer_1 = PdfWriter()
last_page = pdf_reader.pages[-1]
pdf_writer_1.add_page(last_page)

with Path("last_page.pdf").open(mode="wb") as output_file_1:
    pdf_writer_1.write(output_file_1)

# 2
pdf_writer_2 = PdfWriter()
num_of_pages = len(pdf_reader.pages)
for i in range(1, num_of_pages):
    if i % 2 != 0:  # guarantees that you pick every even page - (range 1, num_of_pages) i:1 -> page:0
        page = pdf_reader.pages[i]
        pdf_writer_2.add_page(page)

with Path("every_other_page.pdf").open(mode="wb") as output_file_2:
    pdf_writer_2.write(output_file_2)

# 3
pdf_writer_3 = PdfWriter()
pdf_writer_4 = PdfWriter()
for page in pdf_reader.pages[:150]:
    pdf_writer_3.add_page(page)
for page in pdf_reader.pages[150:]:
    pdf_writer_4.add_page(page)

with Path("part_first.pdf").open(mode="wb") as output_file_3:
    pdf_writer_3.write(output_file_3)
with Path("part_second.pdf").open(mode="wb") as output_file_4:
    pdf_writer_4.write(output_file_4)