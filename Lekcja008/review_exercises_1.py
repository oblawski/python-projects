from pathlib import Path
from PyPDF2 import PdfReader
from PyPDF2 import PdfWriter

#1
path_file = (Path.home()/"OneDrive"/"Pulpit"/"ZR"/"Lekcja008"/"practice_files"/"zen.pdf")
pdf_reader = PdfReader(path_file)

#2
num_of_pages = len(pdf_reader.pages)
print(num_of_pages)

#3
text = pdf_reader.pages[0]
print(text.extract_text())