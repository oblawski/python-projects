from pathlib import Path
from PyPDF2 import PdfReader

pdf_path = (Path.home()/"OneDrive"/"Pulpit"/"ZR"/"Lekcja008"/"practice_files"/"Pride_and_Prejudice.pdf")

# 1
pdf_reader = PdfReader(pdf_path)
# creating .txt file in directory
output_file_path = Path.home()/"OneDrive"/"Pulpit"/"ZR"/"Lekcja008"/"practice_files"/"Pride_and_Prejudice.txt"

# 2
# write to file.txt matedata and whole pdf text
with output_file_path.open(mode="w") as output_file:
    # 3
    title = pdf_reader.metadata.title
    num_pages = len(pdf_reader.pages)
    output_file.write(f"{title}\nNumber of pages: {num_pages}\n\n")
# 4
    for page in pdf_reader.pages:
        text = page.extract_text()
        output_file.write(text)
