from PyPDF2 import PdfReader
from PyPDF2 import PdfWriter
from pathlib import Path

pdf_path = (Path.home() / "OneDrive" / "Pulpit" / "ZR" / "Lekcja008" / "practice_files" / "Pride_and_Prejudice.pdf")
pdf = PdfReader(pdf_path)
# number_of_pages = len(pdf.pages)
# print(number_of_pages)
#
# print(pdf.metadata.title)  # shows metadata from pdf (title, author etc)
#
# first_page = pdf.pages[0]  # variable that contains first page of the pdf
#
# print(first_page.extract_text())  # shows first page
#
# for page in pdf.pages:
#     print(page.extract_text())  # extracts whole text from every page

# pdf_writer = PdfWriter()
# pdf_writer.add_blank_page(width=72, height=72) # creates single blank page
# with Path("blank.pdf").open(mode="wb") as output_file: # creates blank.pdf
#     pdf_writer.write(output_file)

# first_page = pdf.pages[0] # assigns first page of pdf to variable
pdf_writer = PdfWriter()
# pdf_writer.add_page(first_page) # adds page to pdf_writer object

# with Path("first_page.pdf").open(mode="wb") as output_file: # writes pdf file based on pdf_writer object
#     pdf_writer.write(output_file)

# for n in range(1, 4): #you can loop over numbers of pages
#     page = pdf.pages[n]
#     pdf_writer.add_page(page)

# for page in pdf.pages[1:4]:
#     pdf_writer.add_page(page)
#
# with Path("first_chapter.pdf").open(mode="wb") as output_file:
#     pdf_writer.write(output_file)

pdf_writer.append_pages_from_reader(pdf)

print(len(pdf_writer.pages))