from helpers.string import shout
from helpers.math import area

length = 5
width = 8
word = f"the area of {length}-by-{width} rectangle is {area(length, width)}"
print(shout(word))
