from adder import add, double
from greeter import greet

value = add(2, 2)
print(value)

double_value = double(value)
print(double_value)

greet("Real Python")
